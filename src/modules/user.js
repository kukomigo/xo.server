var _ = require('lodash');
var util = require('./util.js')();

module.exports = function() {
  var users = {};

  return {

    /**
     * Register new user
     *
     * @method register
     * @param {Object} p
     * @return {String}
     */
    register: function(p, session) {
      var valid = true;

      if (p && p.name) {

        // Validate sessions.
        _.each(users, function(user) {
          if (user.session === session) {
            valid = false;
          }
        });

        if (!valid) {
          return false;
        }

        // Generate GUID.
        p.uid = util.guid();
        p.role = 'visitor';
        p.session = session;
        users[p.uid] = p;
        return this.get(p.uid);
      }

      return false;
    },

    /**
     * Login current user
     *
     * @method login
     * @param {String} uid
     * @param {String} session Socket session value
     * @return {Object}
     */
    login: function(uid, session) {
      if (users[uid]) {
        users[uid].session = session;
        return _.pick(users[uid], 'uid', 'name');
      }

      return false;
    },

    /**
     * Logout current user
     *
     * @method logout
     * @param {String} session Socket session value
     * @return {Object}
     */
    logout: function(session) {
      var result = false;

      _.each(users, function(user) {
        if (user.session === session) {
          user.session = '';
          result = user;
        }
      });

      return result;
    },

    /**
     * Get current user
     *
     * @method get
     * @param {String} uid
     * @return {Object}
     */
    get: function(uid) {
      if (uid) {
        return _.omit(users[uid], 'session');
      }
      return this.transform();
    },

    /**
     * Transform all data to strip message before sending it to client.
     * 
     * @return {Object}
     */
    transform: function() {
      var result = {};

      _.each(users, function(item, key) {
        result[key] = _.omit(item, 'session');
      });

      return result;
    },

    /**
     * Update current user
     *
     * @method set
     * @param {String} uid
     * @param {Object} p
     * @return {Object}
     */
    set: function(uid, p) {
      if (uid && p && p.uid === undefined) {
        if (p.role) {
          users[uid].role = p.role;
        }
        if (p.name) {
          users[uid].name = p.name;
        }
        return (p.role || p.name) ? true : false;
      }
      return false;
    },

    /**
     * Remove current user
     *
     * @method remove
     * @param {String} uid
     */
    remove: function(uid) {
      delete users[uid];
    },

    /**
     * Play the game
     *
     * @method play
     * @param {String} uid
     * @return {String|Boolean}
     */
    play: function(uid) {
      var role = 'host';
      if (_.size(users) > 0) {
        _.each(users, function(user) {
          if (user.role === 'host') {
            role = 'guest';
          }
          if (user.role === 'guest') {
            role = false;
          }
        });
        if (role) {
          this.set(uid, { role: role });
        }
        return role;
      }
      return false;
    },

    /**
     * Stop the game
     *
     * @method stop
     * @param {String} uid GUID representing specific user
     */
    stop: function(uid) {
      if (uid) {
        users[uid].role = 'visitor';
      } else {
        _.each(users, function(user) {
          user.role = 'visitor';
        });
      }
    }

  };

};