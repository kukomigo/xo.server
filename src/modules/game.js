var _ = require('lodash');

var util = require('./util.js')();

module.exports = function(user) {

  var games = {};


  /**
   * Rotate array by 45 degrees.
   *
   * @method rotate45
   * @param {Array} arr
   * @returns {Array}
   */
  var rotate45 = function(arr) {
    var index = null;
    var result = [];

    _.forEach(arr, function(row, rowIndex) {
      _.forEach(row, function (value, columnIndex) {
        index = columnIndex + rowIndex;
        if ( ! result[index]) {
          result[index] = [value];
        } else {
          result[index].push(value);
        }
      });
    });

    return result;
  };

  /**
   * Rotate array by 90 degrees
   *
   * @method rotate90
   * @param {Array} arr
   * @returns {Array}
   */
  var rotate90 = function(arr) {
    var result = [];

    _.forEach(arr, function(row, rowIndex) {

      _.forEach(row, function (value, columnIndex) {
        if ( ! result[columnIndex]) {
          result[columnIndex] = [value];
        } else {
          result[columnIndex].push(value);
        }
      });
    });

    return result;
  };

  /**
   * Rotate array by 135 degrees
   *
   * @method rotate135
   * @param {Array} arr
   * @returns {Array}
   */
  var rotate135 = function(arr) {
    arr = arr.reverse();

    return rotate45(arr);
  };

  /**
   * Check table for five consecutive signs in a row.
   *
   * @method checkTable
   * @param arr
   * @returns {boolean} || {Object}
   */
  var checkTable = function(arr) {
    var sign = '';
    var positions = [];
    var result = false;

    _.forEach(arr, function(row, rowIndex) {
      if ( ! result) {
        sign = '';
        positions = [];

        _.forEach(row, function (cell, columnIndex) {
          if ( ! result) {
            if (cell.value === 'X' || cell.value === 'O') {
              if (cell.value === sign) {
                positions.push(cell);
              } else {
                sign = cell.value;
                positions = [cell];
              }
              if (positions.length >= 5) {
                result = {
                  'sign': sign,
                  'positions': positions
                };
              }
            } else {
              sign = '';
              positions = [];
            }
          }
        });
      }
    });

    return result;
  };

  return {

    start: function(uid) {
      var result = false, gid = util.guid();

      if (user.get(uid)) {
        result = {
          gid: gid,
          host: uid,
          turn: uid,
          board: this.createBoard(20, 20)
        };

        games[gid] = result;
        user.play(uid);
      }

      return result;
    },

    createBoard: function(x, y) {
      var rowIndex, columnIndex, arr, result = [];

      if (x !== null) {
        for (rowIndex = 0; rowIndex < y; rowIndex++) {
          arr = [];
          for (columnIndex = 0; columnIndex < x; columnIndex++) {
            arr[columnIndex] = {
              'row': rowIndex,
              'column': columnIndex,
              'value': ''
            };
          }
          result[rowIndex] = arr;
        }
      }
      return result;
    },

    join: function(gid, uid) {
      var result = false;

      if (user.get(uid) && games[gid]) {
        games[gid].guest = uid;
        user.play(uid);
        result = games[gid];
      }

      return result;
    },

    reset: function(gid) {
      var result = false;

      if (games[gid]) {
        user.stop(games[gid].host);
        if (games[gid].guest) {
          user.stop(games[gid].guest);
        }
        result = _.pick(games[gid], 'gid', 'host', 'guest');
        delete games[gid];
      }

      return result;
    },

    search: function(uid) {
      var result = {};

      _.each(games, function(game) {
        if (game.host === uid || game.guest === uid) {
          result = game;
        }
      });

      return result;
    },

    get: function(gid) {
      if (gid) {
        return games[gid];
      }
      return this.transform();
    },

    /**
     * Transform all data to strip message before sending it to client.
     * 
     * @return {Object}
     */
    transform: function() {
      var result = {};

      _.each(games, function(item, key) {
        result[key] = _.pick(item, 'gid', 'host', 'guest');
      });

      return result;
    },

    mark: function(gid, uid, x, y) {
      var result = false;

      if (games[gid]) {

        result = { gid: gid, x: x, y: y };

        if (uid === games[gid].host) {
          result.sign = 'X';
          result.turn = games[gid].guest;
        } else if (uid === games[gid].guest) {
          result.sign = 'O';
          result.turn = games[gid].host;
        } else {
          return false;
        }

        games[gid].board[x][y].value = result.sign;
      }

      return result;
    },

    /**
     * Check table for five consecutive signs in any direction.
     *
     * @method result
     * @param gid
     * @returns {boolean} || {Object}
     */
    result: function(gid) {
      var arr0 = games[gid].board.slice(0); //clone board
      var arr45 = [], arr90 = [], arr135 = [];
      var result;

      // 1st check
      result = checkTable(arr0);

      if ( ! result) {
        // rotate array by 90 degrees and 2nd check
        arr90 = rotate90(arr0);
        result = checkTable(arr90);
      }

      if ( ! result) {
        // rotate array by 45 degrees and 3rd check
        arr45 = rotate45(arr0);
        result = checkTable(arr45);
      }

      if ( ! result) {
        // rotate array by 135 degrees and 4th check
        arr135 = rotate135(arr0);
        result = checkTable(arr135);
      }

      if (result) {
        result.winner = result.sign === 'X' ? games[gid].host : games[gid].guest;
      }

      return result;
    }
  };

};