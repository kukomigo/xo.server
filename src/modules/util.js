module.exports = function() {
  
  return {

    /**
     * GUID generator.
     *
     * @private
     * @method guid
     * @return {String} Random guid
     */
    guid: function() {
      var S4 = function() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      };
      return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }

  };

};