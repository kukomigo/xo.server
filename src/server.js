var express = require('express');
var cors = require('cors');
var _ = require('lodash');
var app = express();
var io = require('socket.io').listen(app.listen(1337));

var user = require('./modules/user.js')();
var game = require('./modules/game.js')(user);

app.use(cors());

console.log('Server running at http://127.0.0.1:1337/');


//
// Router
// -----------------------------------------

io.sockets.on('connection', function(socket) {
  console.log('Welcome!', socket.id);


  //
  // User
  // -------------------------------------------

  /**
   * Event browser.ready called when certain client opens our website
   * and has stored data about last user activity.
   *
   * @async
   * @event browser.ready
   * @param  {Object} data Object containing previously stored user UID
   */
  socket.on('browser.ready', function(data) {
    var result = {
      user: user.login(data.uid, socket.id),
      users: user.get(),
      game: game.search(data.uid),
      games: game.get()
    };

    socket.emit('browser.ready.done', result);
    if (result.user) {
      io.emit('user.login.global', result.users[result.user.uid]);
    }
    console.log('Is user registered?:', result.user);
  });

  /**
   * Event disconnect called each time certain client closes browser or
   * changes our page.
   * 
   * @async
   * @event disconnect
   */
  socket.on('disconnect', function() {
    var result = user.logout(socket.id);

    if (result) {
      io.emit('user.logout.global', result);
    }

    console.log('Got disconnect!', socket.id, result);
  });

  /**
   * Event user.register called when user is new to the website and wants to
   * send us his name.
   *
   * @async
   * @event user.register
   * @param  {Object} data Object containing client's name
   * @return {[type]}      [description]
   */
  socket.on('user.register', function(data) {
    var result = user.register(data, socket.id);

    if (result) {
      socket.emit('user.register.done', result);
      io.emit('user.register.done.global', result);
    }

    console.log('Registered as:', result);
  });


  //
  // Game
  // -------------------------------------------

  socket.on('game.start', function(data) {
    var result = game.start(data.uid);

    if (result) {
      socket.emit('game.start.done', result);
      io.emit('game.start.done.global', _.pick(result, 'gid', 'host', 'guest'));
    }

    console.log('Start the game', data, result);
  });

  socket.on('game.join', function(data) {
    var result = game.join(data.gid, data.uid);

    if (result) {
      socket.emit('game.join.done', result);
      io.emit('game.join.done.global', _.pick(result, 'gid', 'host', 'guest'));
    }

    console.log('Joined the game', data, result);
  });

  socket.on('game.reset', function(data) {
    var result = game.reset(data.gid);

    if (result) {
      io.emit('game.reset.done.global', result);
    }

    console.log('Reset the game', data, result);
  });

  socket.on('game.mark', function(data) {
    var markResult = game.mark(data.gid, data.uid, data.x, data.y);
    var gameResult = game.result(data.gid);

    if (markResult) {
      io.emit('game.mark.done.global', markResult, gameResult);
    }

    console.log('Mark on the board', data, markResult);
  });

});
