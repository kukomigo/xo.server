module.exports = function(grunt) {
  'use strict';

  // Load all npm dependencies
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),


    //
    // Configure projects validators
    // -----------------------------------------

    jshint: {
      dev: {
        jshintrc: true,
        src: ['src/**/*.js']
      }
    },


    //
    // Configure watching project's development
    // -----------------------------------------

    express: {
      development: {
        options: {
          script: 'src/server.js'
        }
      }
    },

    watch: {
      app: {
        files: ['src/**/*.js'],
        tasks: ['express'],
        options: {
          spawn: false
        }
      }
    },


    //
    // Configure installing project's files
    // -----------------------------------------

    clean: {
      development: ['temp'],
      production: ['dist']
    },


    //
    // Configure project's development flow
    // -----------------------------------------

    simplemocha: {
      development: {
        options: {
          globals: ['expect']
        },
        src: 'test/**/*.js'
      }
    }

  });


  //
  // Register all important tasks
  // -----------------------------------------

  // I'm pretty out of habit to use this
  grunt.registerTask('lint', ['jshint']);

  // Open server and listen to all changes
  grunt.registerTask('run', ['express', 'watch']);

  // Open server and listen to all changes, but this time
  // lint all changes before updating server
  grunt.registerTask('run-lint', ['lint', 'express', 'watch']); 

  // Run tests
  grunt.registerTask('test', ['lint', 'simplemocha']);

  // Default task
  grunt.registerTask('default', ['lint']);

};
