/**
 * Mocha tests
 * See: http://chaijs.com/api/bdd/
 */

var expect = require('chai').expect;
var _ = require('lodash');
var users = require('../app/modules/users.js')();
var testUser1 = '';
var testUser2 = '';
var testUser3 = '';

describe("Users module", function() {
  "use strict";

  it('should give us empty array while asking for a user', function() {
    var list = users.get();
    expect(list).to.be.an('object');
    expect(list).to.deep.equal({});
  });
 
  it('should fail register new user when no data passed', function() {
    var newUser = users.register();
    expect(newUser).to.equal(false);
  });

  it('should register new user', function() {
    var newUser = users.register({ name: 'Lukas' });

    testUser1 = newUser.uid;

    expect(newUser).to.have.a.property('uid');
    expect(newUser).to.have.a.property('name');
    expect(newUser).to.have.a.property('role');
  });

  it('should give us newUser data', function() {
    var user = users.get(testUser1);

    expect(user).to.have.a.property('uid');
    expect(user).to.have.a.property('name');
    expect(user).to.have.a.property('role');
    expect(user.name).to.equal('Lukas');
  });

  it('should give us ability to edit user data', function() {
    var data = { name: 'Daniel', role: 'gamer' };
    var result = users.set(testUser1, data);
    var user = users.get(testUser1);

    expect(user.name).to.equal('Daniel');
    expect(user.role).to.equal('gamer');
    expect(result).to.equal(true);
  });

  it('should be able to stop any current games', function() {
    var user = users.get(testUser1);

    expect(user.role).to.equal('gamer');

    users.stop();

    expect(user.role).to.equal('visitor');
  });

  it('should be able to play the game', function() {
    testUser2 = users.register({ name: 'Wojtek' }).uid;
    testUser3 = users.register({ name: 'Bartek' }).uid;

    var result1 = users.play(testUser2);
    var result2 = users.play(testUser3);
    var result3 = users.play(testUser1);

    expect(result1).to.equal('host');
    expect(result2).to.equal('guest');
    expect(result3).to.equal(false);
  });

  it('should give us proper enemy data', function() {
    var result1 = users.enemy(testUser1);
    var result2 = users.enemy(testUser2);
    var result3 = users.enemy(testUser3);

    expect(result1).to.equal(false);
    expect(result2.name).to.equal('Bartek');
    expect(result3.name).to.equal('Wojtek');
  });

  it('should remove user', function() {
    users.remove();
    expect(_.size(users.get())).to.equal(3);

    users.remove('555');
    expect(_.size(users.get())).to.equal(3);

    users.remove(testUser2);
    expect(_.size(users.get())).to.equal(2);
  });
});