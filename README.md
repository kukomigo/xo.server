# Noguhts and Crosses Server - javascript online game

Project uses bpflow boilerplate described below.

## Setup

First make sure you have installed node.js >= 0.10 and grunt-cli.
Clone this repo to your folder.
Hit ```npm install``` to install all dev dependencies for grunt.
Great, that's it! Now you're ready to develop and play!

## Tasks

Thanks to grunt and many plugins we can buid our projects fater.

Check you coffee and less files:
```
$ grunt lint
```

Run development flow:
```
$ grunt run
```

Run your tests:
Check you coffee and less files:
```
$ grunt test
```

## TODO

Building process! Minify and concat files.
